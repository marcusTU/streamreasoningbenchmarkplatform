package com.srbp.service;


import cern.colt.list.DoubleArrayList;
import cern.jet.random.AbstractDistribution;
import cern.jet.random.Poisson;
import cern.jet.random.engine.MersenneTwister;
import cern.jet.random.engine.RandomEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * generates the random distribution for creating the packages
 *
 * @author Marcus
 *
 */
public class RandomNumberService {

    /**
     * log4j
     */
    private static final Logger LOG = LoggerFactory.getLogger(RandomNumberService.class);

    /**
     * Constructor
     */
    public RandomNumberService() {
    }

    /**
     * mean is fixed to 10 in the randomNumberGenerator distribution is fixed to
     * Poisson
     *
     * @param seed
     * @param randomNumberListSize
     * @param mean
     * @return
     */
    public DoubleArrayList randomNumberGenerator(int seed, int randomNumberListSize, long mean) {
        //int mean = 10;//generators a Poisson distribution with mean 1, average rate of success
        // for tests and debugging use a random engine with CONSTANT seed --> deterministic and reproducible results
        //according to Shawn J. Cokus(http://acs.lbl.gov/ACSSoftware/colt/api/cern/jet/random/engine/MersenneTwister.html)
        //use odd seed
        RandomEngine engine = new MersenneTwister(seed);
        // your favourite distribution goes here
        AbstractDistribution dist = new Poisson(mean, engine);
        // collect random numbers in a list 
        DoubleArrayList numberList = new DoubleArrayList(randomNumberListSize);
        for (int i = 0; i < randomNumberListSize; i++) {
            numberList.add(dist.nextDouble());
        }
        //LOG.info("successfully created random number list of size: " + randomNumberListSize + " with seed: " + seed);
        return numberList;
    }

}
