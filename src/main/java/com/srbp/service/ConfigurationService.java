package com.srbp.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import com.srbp.datagen.model.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service for generating, loading and storing a configuration file for the
 * {@link RoomDataGenerator}
 *
 * Example usage.  <code>
 * config.createConfigurationFile(new Configuration(2, 2, new Long(2), 1.0, 2, "asdfasd", "2", 60.0));
 * </code>
 *
 * @author Marcus
 * @author Peter
 * @author Bernhard
 */
public class ConfigurationService {
    
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationService.class);

    /**
     * creates the properties file from a Configuration object
     *
     * @param config
     */
    public void createConfigurationFile(Configuration config) {
        Properties prop = new Properties();
        OutputStream output = null;
        String filePath = "ConfigurationFiles//";
        try {
            // set the properties value
            prop.setProperty("roomNumber", String.valueOf(config.getRoomNumber()));
            prop.setProperty("personNumber", String.valueOf(config.getPersonNumber()));
            prop.setProperty("interArrivalTime", String.valueOf(config.getInterArrivalTime()));
            prop.setProperty("randomNumberGeneratorChooser", String.valueOf(config.getRandomNumberGeneratorChooser()));
            prop.setProperty("seedChooser", String.valueOf(config.getSeedChooser()));
            prop.setProperty("query", String.valueOf(config.getQuery()));
            prop.setProperty("engine", String.valueOf(config.getEngine()));
            prop.setProperty("duration", String.valueOf(config.getDuration()));
            prop.setProperty("creationTime", String.valueOf(config.getCreationTime()));
            filePath = filePath + createFileName();
            output = new FileOutputStream(filePath);
            prop.store(output, null);
        } catch (IOException io) {
            LOG.warn(io.getMessage(), io);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    LOG.warn(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * create file Name for config file
     *
     * @return
     */
    private String createFileName() {
        DateFormat df = new SimpleDateFormat("MM-dd-yyyy_HH-mm-ss_");
        // Get the date today using Calendar object
        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(today);
        return "config_" + reportDate + ".properties";
    }
    
}
