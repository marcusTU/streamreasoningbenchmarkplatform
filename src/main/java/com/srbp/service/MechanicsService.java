package com.srbp.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.srbp.datagen.RoomDataGenerator;
import com.srbp.datagen.model.Configuration;
import com.srbp.datagen.model.RoomOntology;
import com.srbp.engines.EngineFactory;
import com.srbp.engines.IEngine;
import com.srbp.engines.SupportedEngines;
import com.srbp.engines.measurement.InstrumentedThreadPool;
import com.srbp.engines.measurement.MeasureRepository;
import com.srbp.util.listener.AggregationListener;
import com.srbp.util.listener.ConsoleListener;
import com.srbp.util.listener.CorrectnessListener;
import com.srbp.util.listener.RDFEvent;
import com.srbp.util.queries.AbstractQuery;
import com.srbp.util.queries.Query1;
import com.srbp.util.queries.Query2;
import com.srbp.util.queries.Query3;

/**
 * This class starts the engine, data generator and instruments the measurement
 * routines.
 *
 * @author Marcus
 * @author Bernhard
 */
public class MechanicsService {

    private static final Logger log = LoggerFactory.getLogger(MechanicsService.class);

    /**
     * interval of measurements
     */
    public static long MEASURE_INTERVAL = 1000L;

    /**
     * list containing strings of room-stream uris
     */
    public ArrayList<String> streamURIList = new ArrayList<String>();

    /**
     * Maximum measurment interval, normalized to miliseconds.
     */
    public static long MAX_INTERVAL = 60000L;

    public static boolean hasStarted = false;

    private static ExecutorService generatorThreadpool = Executors.newCachedThreadPool();

    /**
     * Starts the engine and measurements services.
     */
    public void runMechanics(Configuration c) {

        /**
         * WARM START This is where the action happens. For COLD StART see
         * threadPool.
         */
        Configuration config = c;

        MAX_INTERVAL = (long) config.getDuration() * 1000;
        MEASURE_INTERVAL = (long) Math.ceil(MAX_INTERVAL / 25);

        if (generatorThreadpool.isShutdown()) {
            generatorThreadpool = Executors.newCachedThreadPool();
        }

        /**
         * 1. build streamURIlist (used for building the queries and for
         * instantiating the data generators) + create data generator instances
         * based on room number
         */
        for (int i = 1; i <= config.getRoomNumber(); i++) {
            streamURIList.add(RoomOntology.NS + "streams/room" + String.valueOf(i) + "/");
            log.info("starting Room Data Generator with IRI: " + streamURIList.get(i - 1));
            RoomDataGenerator rdg = new RoomDataGenerator(config.getPersonNumber(), config.getInterArrivalTime(),
                    c.getInterArrivalTimeSeed(), c.getPersonChoiceSeed(), streamURIList.get(i - 1), config.getEngine(), MAX_INTERVAL, i);
            RandomNumberService randGenerator = new RandomNumberService();
            rdg.setRandomGenerator(randGenerator);
            // room data generator thread
            generatorThreadpool.execute(rdg);
        }
        log.info("Room Data Generator(s) started...");
        /**
         * 2. start engine
         */
        final IEngine engine = EngineFactory.getEngine(SupportedEngines.valueOf(c.getEngine()));
        final BlockingQueue<Runnable> engines = new ArrayBlockingQueue<>(1);
        engines.add(engine);
        final InstrumentedThreadPool itp = new InstrumentedThreadPool(engines);
        log.info("Start streaming engine ...");
        itp.execute(engine);
        hasStarted = true;
        log.info("Streaming engine started ...");

        /**
         * 3. register query
         */
        final AggregationListener throughput = new AggregationListener(RDFEvent.THROUGHPUT, MEASURE_INTERVAL, MeasureRepository
                .getInstance().getThroughputChart());
        final AggregationListener queueSize = new AggregationListener(RDFEvent.QUEUESIZE, MEASURE_INTERVAL, MeasureRepository.getInstance()
                .getQueueSizeChart());
        // final CSVListener list = new CSVListener();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final CorrectnessListener correctnessListener = new CorrectnessListener(c.getQuery(), config.getRoomNumber());
        try {
            log.info("Registering query and listeners...");
            engine.registerSelectQuery(engine.getQuery(streamURIList, c.getQuery()), streamURIList);
            engine.registerEventListener(new ConsoleListener());
            engine.registerEventListener(correctnessListener);
            engine.registerMeasurementListener(throughput);
            engine.registerMeasurementListener(queueSize);
            // engine.registerMeasurementListener(list);
            log.info("Queries and listeners registered...");
        } catch (ParseException e1) {
            log.error(e1.getMessage(), e1);
        }
        Timer stop = new Timer();
        stop.schedule(new TimerTask() {

            @Override
            public void run() {
                log.info("STOPPING EVERYTHING");
                boolean validate = correctnessListener.isValid();
                log.info("Valid " + validate);
                // generatorThreadpool.shutdown();
                // generatorThreadpool.shutdownNow();
                try {
                    if (generatorThreadpool.awaitTermination(1, TimeUnit.SECONDS)) {
                        System.out.println("task completed");
                    } else {
                        System.out.println("Forcing shutdown...");
                        generatorThreadpool.shutdownNow();
                    }
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                engine.stopThread();
                itp.shutdown();
                itp.shutdownNow();
                engines.clear();
                // list.close();
            }

        }, MAX_INTERVAL);

        log.info("End...");
    }

    /**
     * Loads the properties file and returns a config Object.
     *
     * @return
     */
    private Configuration extractConfigurations() {
        Properties prop = new Properties();
        String propFile = getLastModifiedFileName("../ConfigurationFiles//");

        InputStream propStream = null;
        try {
            // load properties from file
            log.info("Loading File: " + propFile);
            propStream = MechanicsService.class.getResourceAsStream("../ConfigurationFiles//" + propFile);
            prop.load(propStream);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (propStream != null) {
                try {
                    propStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        String query = prop.getProperty("query");

        AbstractQuery qry;

        if (query.equals("Query1")) {
            qry = new Query1();
        } else if (query.equals("Query2")) {
            qry = new Query2();
        } else {
            qry = new Query3();
        }

        Configuration config = new Configuration(Integer.parseInt(prop.getProperty("roomNumber")), Integer.parseInt(prop
                .getProperty("personNumber")), Long.parseLong(prop.getProperty("interArrivalTime")), Double.parseDouble(prop
                        .getProperty("randomNumberGeneratorChooser")), Integer.parseInt(prop.getProperty("seedChooser")),
                qry, prop.getProperty("engine"), Double.parseDouble(prop.getProperty("duration")));
        return config;
    }

    /**
     * gets the last modified files in a directory
     *
     * @param directory
     * @return
     */
    private String getLastModifiedFileName(String directory) {
        URL url = MechanicsService.class.getResource(directory);
        try {
            File dir = new File(url.toURI());
            File[] files = dir.listFiles();
            if (files.length == 0) {
                return null;
            }
            File lastModifiedFile = files[0];
            for (File file : files) {
                // check the last Modified files
                if (lastModifiedFile.lastModified() < file.lastModified()) {
                    lastModifiedFile = file;
                }
            }
            return lastModifiedFile.getName();
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
