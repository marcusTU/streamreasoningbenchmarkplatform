/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srbp.datagen.model;

import cern.colt.list.AbstractCollection;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Iterator for iterating over the colt stuff.
 *
 * @author Bernhard
 * @param <E>
 */
public class RandomIterator<E> implements Iterator<E> {

    private AbstractCollection coll = null;

    /**
     * Initalizes the iterator with an {@link AbstractCollection} of Colt Library.
     * @param collection 
     */
    public RandomIterator(AbstractCollection collection) {
        this.coll = collection;
    }

    private int idx = 0;

    @Override
    public boolean hasNext() {
        boolean isIterable = (this.coll.toList().size() > idx);
        if (!coll.isEmpty() && isIterable) {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
	@Override
    public E next() {
        List<?> list = this.coll.toList();
        if (list.size() > idx) {
            E entity = (E) list.get(idx);
            idx++;
            return entity;
        } else {
            throw new NoSuchElementException("Size is smaller than requested Element");
        }
    }

    @Override
    public void remove() {
        this.coll.toList().remove(idx);
        idx++;
    }
}
