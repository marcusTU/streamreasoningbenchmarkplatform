/* CVS $Id: $ */
package com.srbp.datagen.model;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Vocabulary definitions from rsp2014-onto_mod_chain.rdf.
 *
 * @author Auto-generated by schemagen on 30 Sep 2014 17:14
 */
public class RoomOntology {

    /**
     * <
     * p>
     * The RDF model that holds the vocabulary terms.</p>
     */
    private static Model m_model = ModelFactory.createDefaultModel();

    /**
     * <
     * p>
     * The namespace of the vocabulary as a string.</p>
     */
    public static final String NS = "http://www.streamreasoning.org/ontologies/sr4ld2013-onto#";

    /**
     * <
     * p>
     * The namespace of the vocabulary as a string.</p>
     *
     * @see #NS
     */
    public static String getURI() {
        return NS;
    }

    /**
     * <
     * p>
     * The namespace of the vocabulary as a resource.</p>
     */
    public static final Resource NAMESPACE = m_model.createResource(NS);

    public static final Resource Observation = m_model.createResource("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#Observation");
    public static final Resource Person = m_model.createResource("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#Person");
    public static final Resource Post = m_model.createResource("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#Post");
    public static final Resource Room = m_model.createResource("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#Room");
    public static final Resource Sensor = m_model.createResource("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#Sensor");
    public static final Property discusses = m_model.createProperty("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#discusses");
    public static final Property isConnectedTo = m_model.createProperty("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#isConnectedTo");
    public static final Property isIn = m_model.createProperty("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#isIn");
    public static final Property isWith = m_model.createProperty("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#isWith");
    public static final Property posts = m_model.createProperty("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#posts");
    public static final Property where = m_model.createProperty("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#where");
    public static final Property who = m_model.createProperty("http://www.streamreasoning.org/ontologies/sr4ld2013-onto#who");

}
