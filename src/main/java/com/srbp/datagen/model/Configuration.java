package com.srbp.datagen.model;

import com.srbp.util.queries.AbstractQuery;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;

import org.springframework.stereotype.Component;

/**
 * Configuration file - creates the Configuration file
 *
 * @author marcus
 */
@Component
public class Configuration {

    /**
     * select room.
     */
    //1a.
    private int roomNumber;

    /**
     * select people.
     */
    //1b.
    private int personNumber;

    /**
     * interArrivalTime
     */
    private long interArrivalTime;

    /**
     * randomNumberGeneratorChooser
     */
    private double randomNumberGeneratorChooser;
    /**
     * seedChooser
     */
    private int seedChooser;
    /**
     * query
     */
    //3.
    private AbstractQuery query;
    /**
     * duration
     */
    //4. 
    private String engine;
    /**
     * duration
     */
    //5. 
    private double duration;
    /**
     * creation Time
     */
    //Configuration we need a deep copy of it.
    private long creationTime;

    /**
     * Is the seed to choose a person.
     */
    private int personChoiceSeed;

    /**
     * interArrivalTimeSeed
     */
    private int interArrivalTimeSeed;

    /**
     * Constructor.
     *
     * @param roomNumber
     * @param personNumber
     * @param interArrivalTime
     * @param randomNumberGeneratorChooser
     * @param seedChooser
     * @param query
     * @param engine
     * @param duration
     */
    public Configuration(int roomNumber, int personNumber, long interArrivalTime,
            double randomNumberGeneratorChooser, int seedChooser, AbstractQuery query,
            String engine, double duration) {
        //### TODO Explain to Bernhard !
        this.roomNumber = roomNumber;
        this.personNumber = personNumber;
        this.interArrivalTime = interArrivalTime;
        this.randomNumberGeneratorChooser = randomNumberGeneratorChooser;
        this.seedChooser = seedChooser;
        this.query = query;
        this.engine = engine;
        this.duration = duration;
        initConfiguration();
    }

    /**
     *
     */
    public Configuration() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Initalizes the configuration.
     */
    private void initConfiguration() {
        this.creationTime = System.currentTimeMillis();
    }

    /**
     * @return the roomNumber
     */
    public int getRoomNumber() {
        return roomNumber;
    }

    /**
     * @param roomNumber the roomNumber to set
     */
    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    /**
     * @return the interArrivalTime
     */
    public long getInterArrivalTime() {
        return interArrivalTime;
    }

    /**
     * @param interArrivalTime the interArrivalTime to set
     */
    public void setInterArrivalTime(long interArrivalTime) {
        this.interArrivalTime = interArrivalTime;
    }

    /**
     * @return the randomNumberGeneratorChooser
     */
    public double getRandomNumberGeneratorChooser() {
        return randomNumberGeneratorChooser;
    }

    /**
     * @param randomNumberGeneratorChooser the randomNumberGeneratorChooser to
     * set
     */
    public void setRandomNumberGeneratorChooser(double randomNumberGeneratorChooser) {
        this.randomNumberGeneratorChooser = randomNumberGeneratorChooser;
    }

    /**
     * @return the seedChooser
     */
    public int getSeedChooser() {
        return seedChooser;
    }

    /**
     * @param seedChooser the seedChooser to set
     */
    public void setSeedChooser(int seedChooser) {
        this.seedChooser = seedChooser;
    }

    /**
     * @return the query
     */
    public AbstractQuery getQuery() {
        return query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(AbstractQuery query) {
        this.query = query;
    }

    /**
     * @return the engine
     */
    public String getEngine() {
        return engine;
    }

    /**
     * @param engine the engine to set
     */
    public void setEngine(String engine) {
        this.engine = engine;
    }

    /**
     * @return the duration
     */
    public double getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(double duration) {
        this.duration = duration;
    }

    /**
     * @return the creationTime
     */
    public Date getCreationTime() {
        return new Date(creationTime);
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime.getTime();
    }

    /**
     * @return the peopleNumber
     */
    public int getPersonNumber() {
        return personNumber;
    }

    /**
     * Sets the number of people in a room.
     *
     * @param personNumber the amount to set
     */
    public void setPersonNumber(int personNumber) {
        this.personNumber = personNumber;
    }

    /**
     * @return the personChoiceSeed
     */
    public int getPersonChoiceSeed() {
        return personChoiceSeed;
    }

    /**
     * @param personChoiceSeed the personChoiceSeed to set
     */
    public void setPersonChoiceSeed(int personChoiceSeed) {
        this.personChoiceSeed = personChoiceSeed;
    }

    /**
     * @return the interArrivalTimeSeed
     */
    public int getInterArrivalTimeSeed() {
        return interArrivalTimeSeed;
    }

    /**
     * @param interArrivalTimeSeed the interArrivalTimeSeed to set
     */
    public void setInterArrivalTimeSeed(int interArrivalTimeSeed) {
        this.interArrivalTimeSeed = interArrivalTimeSeed;
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
