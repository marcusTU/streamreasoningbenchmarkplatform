package com.srbp.datagen.model;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * A RdfQuadruple based on the definition of a {@link Triple}. A Quadruple is a
 * RDF Triple with an long timestamp.
 *
 * @author peter
 * @author bernhard
 * @author Marcus
 */
public class RdfQuadruple extends Triple {

	/**
	 * timestamp of quad.
	 */
    private final long timestamp;
    /**
     * describes the iri of a RDFQuadruple.
     */
    private String iri; 

    /**
     * Constructor. Allows the conversion from a {@link Triple} to a Quadruple.
     * 
     * @param s
     * @param p
     * @param o
     * @param milliseconds
     */
    public RdfQuadruple(final Node s, final Node p, final Node o, final long milliseconds) {
        super(s, p, o);
        this.timestamp = milliseconds;
    }

    /**
     * Empty constructor. Initalized with empty data.
     */
    public RdfQuadruple() {
        super(Node.createLiteral(""), Node.createLiteral(""), Node.createLiteral(""));
        this.timestamp = -1;
    }

    /**
     * Returns the timestamp.
     *
     * @return
     */
    public long getTimestamp() {
        return this.timestamp;
    }

    @Override
    public String toString() {
        return getSubject() + " " + getPredicate() + " " + getObject() + " . (" + Long.toString(getTimestamp()) + ")";
    }


    /**
     * Returns the IRI of a Graph.
     * @return the iri
     */
    public String getIri() {
        return iri;
    }

    /**
     * Sets the IRI of the Graph.
     * @param iri the iri to set
     */
    public void setIri(String iri) {
        this.iri = iri;
    }

    @Override
    public Triple asTriple() {
        return super.asTriple(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o.getClass().equals(Triple.class)) {
            if (this.asTriple().matches((Triple) o)) {
                return true;
            }
        } else {
            RdfQuadruple quad = (RdfQuadruple) o;
            if (this.asTriple().matches(quad.asTriple())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).
                append(this.getSubject()).
                append(this.getPredicate()).
                append(this.getObject()).
                append(this.getTimestamp()).
                toHashCode();
    }
}
