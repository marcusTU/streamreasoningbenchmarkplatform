/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srbp.datagen.model;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import org.deri.cqels.engine.ExecContext;
import org.deri.cqels.engine.RDFStream;

/**
 * Default RDF Stream, used for CSparql.
 * 
 * @author Max
 */
public class DefaultRDFStream extends RDFStream {

    /**
     * Constructer. Derived from {@link RDFStream}.
     * @param context
     * @param uri 
     */
    public DefaultRDFStream(ExecContext context, String uri) {
        super(context, uri);
    }

    /**
     * Streams a jena model.
     * @param t 
     */
    public void stream(Model t) {
        StmtIterator iter = t.listStatements();
        while (iter.hasNext()) {
            super.stream(iter.next().asTriple());
        }
    }

    @Override
    public void stop() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
