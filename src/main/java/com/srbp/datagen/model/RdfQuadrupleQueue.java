/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srbp.datagen.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This is the RdfQuadruple Queue. Currently it is aimed, that only one engine
 * is run simutaneouly.
 *
 * @author Bernhard
 */
public final class RdfQuadrupleQueue extends ArrayList<RdfQuadruple> {

    /**
     * UUID
     */
    private static final long serialVersionUID = -3264859986530974491L;

    private static RdfQuadrupleQueue instance = null;

    private RdfQuadrupleQueue() {
    }

    /**
     * Singleton Constructor, can be called by hand.
     *
     * @return
     */
    public static synchronized RdfQuadrupleQueue getInstance() {
        if (instance == null) {
            instance = new RdfQuadrupleQueue();
        }
        return instance;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(RdfQuadruple e) {
        return super.add(e); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RdfQuadruple get(int index) {
        RdfQuadruple quad = null;
        try {
            quad = super.get(index);
            super.remove(index);
        } catch (Exception ex) {

        }

        return quad; //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return super.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<RdfQuadruple> iterator() {
        return super.iterator(); //To change body of generated methods, choose Tools | Templates.
    }

}
