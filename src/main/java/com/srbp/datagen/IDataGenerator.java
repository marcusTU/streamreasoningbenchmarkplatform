package com.srbp.datagen;

/**
 * Interface for data generators.
 *
 * @author peter
 */
public interface IDataGenerator {

    /**
     * stop generating data.
     */
    void stop();

}
