package com.srbp.datagen;

import com.srbp.datagen.model.RdfQuadruple;
import com.srbp.datagen.model.RdfQuadrupleQueue;
import cern.colt.list.DoubleArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Iterator;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
import com.srbp.datagen.model.RoomOntology;
import com.srbp.datagen.model.RandomIterator;
import com.srbp.service.MechanicsService;
import com.srbp.service.RandomNumberService;

import org.apache.commons.lang3.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * runs the main data generator application.
 *
 * @author marcus
 *
 */
public class RoomDataGenerator implements Runnable, IDataGenerator {

    /**
     * DTO - Objects.
     */
    private RandomNumberService randGenerator;
    private RdfQuadrupleQueue queue;
    private static final transient Logger LOG = LoggerFactory.getLogger(RoomDataGenerator.class);
    private static Iterator<Double> itRndInterArrivalTime;

    /**
     * Variabls used as runtime parameter for the generator.
     */
    private boolean keepRunning = false;
    private long interArrivalTimeMean;
    private int interArrivalTimeSeed;
    private int numPpl;
    private int roomNumber;
    private long timeElapsed;
    /**
     * Data generation variables, such as Rooms, Persons.
     */
    private String iri;
    private int cntTriples = 0;
    private final List<Resource> persons = new ArrayList<>();
    private final List<Resource> rooms = new ArrayList<>();
    private static Model model;
    private static final String PREFIX_PERSON = RoomOntology.NS + "person";
    private static final String PREFIX_ROOM = RoomOntology.NS + "room";

    /**
     * Default constructor. Added for Introspection.
     */
    public RoomDataGenerator() {
    }

    /**
     * Sets the random generator.
     *
     * @param randGenerator
     */
    public void setRandomGenerator(final RandomNumberService randGenerator) {
        this.randGenerator = randGenerator;
    }

    /**
     * Constructor. The parameter are loaded from a property file.
     *
     * @param numPpl
     * @param roomNumber
     * @param interArrivalTimeMean
     * @param interArrivalTimeSeed
     * @param personChoiceSeed
     * @param roomChoiceSeed
     * @param iri
     * @param engine
     * @param duration
     */
    public RoomDataGenerator(int numPpl, long interArrivalTimeMean, int interArrivalTimeSeed, int personChoiceSeed, String iri,
            String engine, double duration, int roomNumber) {
        this.numPpl = numPpl;
        this.interArrivalTimeSeed = interArrivalTimeSeed;
        this.interArrivalTimeMean = interArrivalTimeMean;
        this.iri = iri;
        this.roomNumber = roomNumber;
        this.queue = RdfQuadrupleQueue.getInstance();
        this.randGenerator = new RandomNumberService();
        initGenerator();
    }

    /**
     * Initalizes the generator quasi invariants.
     */
    private void initGenerator() {
        DoubleArrayList rndInterArrivalTimes = randGenerator.randomNumberGenerator(this.interArrivalTimeSeed, 200000,
                this.interArrivalTimeMean);
        if (LOG.isTraceEnabled()) {
            LOG.trace("randomnumbers :" + rndInterArrivalTimes.toString());
        }
        itRndInterArrivalTime = new RandomIterator<>(rndInterArrivalTimes);
        model = ModelFactory.createDefaultModel();
    }

    /**
     * create a random number in a certain interval.
     *
     * @param min
     * @param max
     * @return
     */
    public int randInt(int min, int max) {
        // TODO: use COLT lib here
        Random rand = new Random();
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() {
        LOG.info("Stopping Room Data Generator - Thread");
        keepRunning = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        LOG.info("Starting Room Data Generator - Thread");
        long startTime = System.currentTimeMillis();
        // clear all previous used instances
        model.removeAll();
        persons.clear();
        rooms.clear();

        try {
            keepRunning = true;

            // create static person knowledge
            for (int j = 1; j < this.numPpl + 1; j++) {
                StrBuilder personUri = new StrBuilder(PREFIX_PERSON);
                Resource person = model.createResource(personUri.append(j).toString()).addProperty(RDF.type, RoomOntology.Person);
                persons.add(person);
            }

            // create static room knowledge
            StrBuilder roomUri = new StrBuilder(PREFIX_ROOM);
            Resource room = model.createResource(roomUri.append(this.roomNumber).toString()).addProperty(RDF.type, RoomOntology.Room);

	          while (keepRunning) {

                Resource randomPerson = getPerson();

                // write to file here
                RdfQuadruple q = new RdfQuadruple(randomPerson.asNode(), RoomOntology.isIn.asNode(), room.asNode(),
                        System.currentTimeMillis());
                q.setIri(iri);
                synchronized (this) {
                    LOG.info("put quad into queue: " + q.getSubject().getLocalName() + " - " + q.getObject().getLocalName());
                    queue.add(q);
                }
                this.cntTriples++;

                if (LOG.isDebugEnabled()) {
                    if (cntTriples == 100) {
                        LOG.debug("{} triples generated so far; queue-size {}", cntTriples, queue.size());
                        cntTriples = 0;
                    }
                }

                // make sleep and creation of next triples dependent from
                // interarrival time of triples
                try {
                    Thread.sleep(Math.round(itRndInterArrivalTime.next()));
                } catch (InterruptedException ex) {
                	LOG.info("rdg thread interrupted");
                    this.stop();
                }
            }
        }         
        catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * Returns a person from generated list.
     *
     * @return
     */
    private Resource getPerson() {
        return this.persons.get(randInt(0, this.numPpl - 1));
    }
}
