package com.srbp.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVWriter;

/**
 *
 * @author Marcus
 *
 * Pushes the results output into a csv file.
 */
public class CSVFile {
    
    private static final Logger log = LoggerFactory.getLogger(CSVFile.class);
    
    private static CSVWriter writer = null;
    
    private static String filePath;

    /**
     *
     */
    //invariant: the csv file
    public CSVFile(String writerPath) {
        FastDateFormat fsf = FastDateFormat.getInstance("yyyy-MM-DD_hh-mm");
        filePath = writerPath + "csvFile_" + fsf.format(new Date()) + ".csv";
        init();
    }
    
    private void init() {
        log.info("Creating file under: {}", filePath);
        try {
            writer = new CSVWriter(new FileWriter(filePath), ';');
            String[] entries = {"Measure timestamp", "Throughput"};
            writer.writeNext(entries);
            
        } catch (IOException ex) {
            log.error("Error creating file: {}", filePath);
            log.error(ex.getMessage(), ex);
        }
    }
    
    public void close() {
        try {
            writer.close();
        } catch (IOException ex) {
            log.error("Error closing file: {}", filePath);
            log.error(ex.getMessage(), ex);
        }
    }

    /**
     * Writes the domain object to a csv file for a throughput object.
     *
     * @param writerPath
     * @param list
     * @throws IOException
     */
    public void writeToCSV(Long timestamp, Double value) {
        //header
        List<String> line = new ArrayList<>();
        line.add(timestamp.toString());
        line.add(value.toString());
        writer.writeNext(line.toArray(new String[line.size()]));
    }
}
