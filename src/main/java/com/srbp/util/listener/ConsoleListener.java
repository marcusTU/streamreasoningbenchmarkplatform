/*
 * Copyright 2014 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.util.listener;

import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.srbp.datagen.model.RdfQuadruple;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listener for printing query results to Console.
 * 
 * @author Bernhard
 */
public class ConsoleListener implements IResultListener {

    private static final transient Logger log = LoggerFactory.getLogger(ConsoleListener.class);

    @Override
    public void updateTriple(Triple triple) {
        log.info("triple result: " + triple.getSubject() + "-" + triple.getPredicate() + " - " + triple.getObject());
    }

    @Override
    public void updateMapping(Binding b) {
        Iterator<Var> itr = b.vars();
        while (itr.hasNext()) {
            Var var = itr.next();
            log.info("binding result: " + var.toString() + " - " + b.get(var).toString());
        }
    }

    @Override
    public void insertRawQuadruple(RdfQuadruple quad) {
        log.trace("Quadruple: {}",quad);
    }
}
