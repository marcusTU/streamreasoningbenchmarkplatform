/*
 * Copyright 2014 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.util.listener;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.srbp.datagen.model.RdfQuadruple;
import com.srbp.util.queries.AbstractQuery;
import com.srbp.util.queries.Query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.text.StrBuilder;

/**
 * This class evaluates queries to find a correctness proof.
 *
 * @author Bernhard
 */
public class CorrectnessListener implements IResultListener {

//    private Model queryResults = ModelFactory.createDefaultModel();
    private List<RDFNode> resultURLs = new ArrayList<>();

    private Model baseModel = ModelFactory.createDefaultModel();

    private AbstractQuery query;
    /**
     * Flag is the engine is valid.
     */
    private boolean isValid = false;

    private List<String> rooms = new ArrayList<>();

    public CorrectnessListener(AbstractQuery proof, int rooms) {
        this.query = proof;
        for (int i = 1; i < rooms+1; i++) {
            this.rooms.add("room" + i);
        }
    }

    @Override
    public void updateTriple(Triple triple) {
        Resource res = ResourceFactory.createResource(triple.getSubject().getURI());
        Property prop = ResourceFactory.createProperty(triple.getPredicate().getURI());
        RDFNode node = this.baseModel.asRDFNode(triple.getObject());
        Statement stmt = ResourceFactory.createStatement(res, prop, node);
        this.baseModel.add(stmt);
    }

    @Override
    public void updateMapping(Binding b) {
        Iterator<Var> itr = b.vars();
        int cnt = 0;
        Resource res = null;
        Property prop = null;
        RDFNode node = null;
        if (itr.hasNext()) {
            Var var = itr.next();
            /*
             * Get mapping from the query: q.getBasicGraphPatternString() and store it to jena
             */
            String curNode = b.get(var).toString();
            if (cnt == 0) {
                res = ResourceFactory.createResource(curNode);
            } else if (cnt == 1) {
                prop = ResourceFactory.createProperty(curNode);
            } else {
                node = this.baseModel.asRDFNode(Node.createURI(curNode));
            }

            cnt++;
        }
        Statement stmt = ResourceFactory.createStatement(res, prop, node);
        this.baseModel.add(stmt);
    }

    @Override
    public void insertRawQuadruple(RdfQuadruple quad) {
        Triple t = quad.asTriple();
        this.baseModel.add(baseModel.asStatement(t));
    }

    /**
     * returns the validity for the last evaluation of the window.
     *
     * @return
     */
    public boolean isValid() {
        boolean result = true;
        final AbstractQuery q = this.query;
        final StrBuilder query = new StrBuilder();
        query.append("PREFIX srbp: <http://www.streamreasoning.org/ontologies/sr4ld2013-onto#> ");
        query.append(" SELECT * WHERE { ").append(q.getBGPAsString().replace(Query.BGP_VARIABLE_STRING, "?room")).append(" } ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~>  CHECKING");
        if (!baseModel.isClosed()) {
            final QueryExecution qe = QueryExecutionFactory.create(query.toString(), baseModel);
            ResultSet rs = qe.execSelect();
            while (rs.hasNext()) {
                QuerySolution next = rs.next();
                Resource room = next.get("room").asResource();
                boolean tmp = this.rooms.contains(room.getLocalName());
                if(!result && tmp){
                    return result;
                }else{
                    result = tmp;
                }
            }
        }
        return result;

    }
}
