/*
 * Copyright 2014 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.util.listener;

import java.util.Collections;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.primefaces.model.chart.ChartSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listener Implementation for collecting and aggregating measurement points.
 * Standardizes and aggregated all events to seconds. 
 * ### Todo
 * * remove {@link ChartSeries} because its a ugly mix of View and Component.
 * * implement timer Start on @PostConstruct annotation
 * 
 * @author Bernhard
 */
public class AggregationListener implements IMeasurmentListener{

    private Double curVal = new Double(0);

    private ChartSeries result = new ChartSeries();
    
    private RDFEvent property;
    
    private final long startTime = System.currentTimeMillis();
    
    private static final Logger log = LoggerFactory.getLogger(AggregationListener.class);
    
    public AggregationListener(RDFEvent property, Long interval, ChartSeries chart) {
        this.property = property;
        this.result = chart;
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                log.trace("Sampling measurement");
                //normalize the sampling to seconds
                long msec = Math.round((System.currentTimeMillis() - startTime) / 1000);
                result.getData().put(msec, curVal);
                curVal =  new Double(0);
            }

        }, 0, interval);
    }
    
        
        
    @Override
    public void fireEvent(RDFEvent type, Integer value) {
        if (type.equals(property)) {
            curVal = curVal + value;
        }
    }

    @Override
    public Map<Long, Double> getResult() {
        return Collections.emptyMap();
    }
    
    @Override
    public RDFEvent getType() {
        return property;
    }
    
}
