/*
 * Copyright 2014 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.util.listener;

import com.srbp.util.CSVFile;
import java.util.Collections;
import java.util.Map;

/**
 * This is the listener that saves data as CSV-File. The file is generated in
 * the target folder.
 *
 * @author Bernhard
 * @author marcus
 */
public class CSVListener implements IMeasurmentListener {

    private static CSVFile file;

    public CSVListener() {
    	//todo: make this work on eclipse (for peter)
    	//suggestion by max: Try SomeClass.class.getResourceAsURI().something like getAbsolutePath() or if it's an instance then do someinstance.getClass().the same method()
        file = new CSVFile("./target/");
    }

    @Override
    public void fireEvent(RDFEvent type, Integer value) {
        file.writeToCSV(System.currentTimeMillis(), value.doubleValue());
    }

    @Override
    public Map<Long, Double> getResult() {
        return Collections.emptyMap();
    }

    @Override
    public RDFEvent getType() {
        return RDFEvent.THROUGHPUT;
    }

    //todo think about it to do automatically
    public void close() {
        file.close();
    }

}
