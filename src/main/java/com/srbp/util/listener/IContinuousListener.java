package com.srbp.util.listener;

import java.util.EventListener;

/**
 * Overall interface for every Listener.
 * 
 * @see IMeasurmentListener
 * @see IResultListener
 * @author Bernhard
 */
public interface IContinuousListener extends EventListener{


}
