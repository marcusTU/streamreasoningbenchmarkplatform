package com.srbp.util.listener;

import at.sti2.spark.core.solution.Match;
import at.sti2.spark.core.triple.RDFTriple;
import at.sti2.spark.core.triple.TripleCondition;
import at.sti2.spark.grammar.pattern.Handler;
import at.sti2.spark.handler.SparkwaveHandler;
import at.sti2.spark.handler.SparkwaveHandlerException;
import com.hp.hpl.jena.graph.Node;
import com.srbp.util.listener.IContinuousListener;
import com.srbp.util.listener.IResultListener;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maxim Kolchin
 */
public class SparkwaveResultHandler implements SparkwaveHandler {

    private final static transient Logger log = LoggerFactory.getLogger(
            SparkwaveResultHandler.class);
    private static IResultListener listener;
    private Handler properties;
    
    public static void setListener(IContinuousListener listener) {
        SparkwaveResultHandler.listener = (IResultListener)listener;
    }

    @Override
    public void init(Handler invokerProperties)
            throws SparkwaveHandlerException {
        this.properties = invokerProperties;
    }

    @Override
    public void invoke(Match match) throws SparkwaveHandlerException {
            List<TripleCondition> conditions = properties.getTriplePatternGraph()
                    .getConstruct().getConditions();
            for(TripleCondition c : conditions) {
                RDFTriple t = c.getConditionTriple();
                String subjectStr;
                if(c.isVariableAtSubject()) {
                    subjectStr = match.getVariableBindings().get(
                            varToKey(t.getSubject().formatString())).formatString();
                } else {
                    subjectStr = t.getSubject().formatString();
                }
                String predicateStr;
                if(c.isVariableAtPredicate()) {
                    predicateStr = match.getVariableBindings().get(
                            varToKey(t.getPredicate().formatString())).formatString();
                } else {
                    predicateStr = t.getPredicate().formatString();
                }
                String objectStr;
                if(c.isVariableAtObject()) {
                    objectStr = match.getVariableBindings().get(
                            varToKey(t.getObject().formatString())).formatString();
                } else {
                    objectStr = t.getObject().formatString();
                }
                Node object;
                if(objectStr.startsWith("<")) {
                    object = Node.createURI(objectStr);
                } else {
                    object = Node.createLiteral(t.getObject().formatString());
                }
                listener.updateTriple(new com.hp.hpl.jena.graph.Triple(
                        Node.createURI(subjectStr),
                        Node.createURI(predicateStr),
                        object));
            }
    }
    
    private String varToKey(final String var) {
        return var.substring(1);
    }
}
