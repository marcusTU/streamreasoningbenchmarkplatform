package com.srbp.util.queries;


/**
 * The factory that returns a certain query.
 * 
 * @author peter
 */
public class QueryFactory {

	private QueryFactory() {
	}

	public static AbstractQuery getQuery(AvailableQueries criteria) {
		switch (criteria) {
		case QUERY1:
			return new Query1();
		case QUERY2:
			return new Query2();
		case QUERY3:
			return new Query3();
		default:
			return null;
		}
	}
}
