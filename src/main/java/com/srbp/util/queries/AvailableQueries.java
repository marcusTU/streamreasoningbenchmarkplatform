/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srbp.util.queries;

/**
 * Enumeration for available queries
 * @author Peter
 */
public enum AvailableQueries {
    QUERY1, 
    QUERY2,
    QUERY3
}
