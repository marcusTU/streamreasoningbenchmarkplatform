/*
 * Copyright 2014 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.util.queries;

/**
 * Query 3: Are the engines scaling with the amount of windows?
 *
 * @author Bernhard
 */
public class Query3 extends AbstractQuery {

    private static Query1 query;

    private String id = "Query3";

    public Query3() {
        query = new Query1();
    }

    @Override
    public String getCQELSQueryString() {
        return query.getCQELSQueryString();
    }

    @Override
    public String getCSPARQLQueryString() {
        return query.getCSPARQLQueryString();
    }

    @Override
    public String getSparkwaveQueryString() {
        return query.getSparkwaveQueryString();
    }

    @Override
    public String getBGPAsString() {
        return "srbp:person1 srbp:isIn ?room .";
    }

    @Override
    public String toString() {
        return id;
    }
}
