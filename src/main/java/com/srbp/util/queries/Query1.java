package com.srbp.util.queries;

/**
 * @author peter
 *
 */
public class Query1 extends AbstractQuery {

    private String id = "Query1";

    public Query1() {
    }

    /* (non-Javadoc)
     * @see com.srbp.util.queries.IQuery#getCQELSQueryString()
     */
    @Override
    public String getCQELSQueryString() {
        return "PREFIX srbp: <http://www.streamreasoning.org/ontologies/sr4ld2013-onto#> "
                + "SELECT "
                + Query.BGP_VARIABLE_STRING + "\n"
                + "WHERE { "
                + Query.STREAM_URI_AND_BGP_DUMMY_STRING
                + "FILTER (?person1 = srbp:person1) "
                + "}";
    }

    /* (non-Javadoc)
     * @see com.srbp.util.queries.IQuery#getCSPARQLQueryString()
     */
    @Override
    public String getCSPARQLQueryString() {
        return "REGISTER QUERY TrackPersonSliding AS "
                + "PREFIX srbp: <http://www.streamreasoning.org/ontologies/sr4ld2013-onto#> "
                + "SELECT "
                + Query.BGP_VARIABLE_STRING + "\n"
                + Query.STREAM_URI_DUMMY_STRING
                + "WHERE { "
                + "{ "
                + Query.BGP_DUMMY_STRING
                + "}}";
    }

    /* (non-Javadoc)
     * @see com.srbp.util.queries.IQuery#getSparkwaveQueryString()
     */
    @Override
    public String getSparkwaveQueryString() {
        return "PREFIX srbp: <http://www.streamreasoning.org/ontologies/sr4ld2013-onto#>\n"
                + "EPSILON_ONTOLOGY = \"target/classes/com/srbp/ontology/rsp2014-onto_mod.ttl\"\n"
                + "STATIC_INSTANCES = \"null\"\n"
                + "HANDLERS {\n"
                + " HANDLER {\n"
                + "     \"class\" = \"com.srbp.util.listener.SparkwaveResultHandler\"\n"
                + " }\n"
                + "}\n"
                + "CONSTRUCT {\n"
                + Query.BGP_DUMMY_STRING
                + "\n} WHERE {\n"
                + Query.BGP_DUMMY_STRING
                + "\nTIMEWINDOW (10000)\n"
                + "}";
    }

    /* (non-Javadoc)
     * @see com.srbp.util.queries.AbstractQuery#getWindowSize()
     */
    @Override
    public String getWindowSize() {
        return "10s";
    }

    /* (non-Javadoc)
     * @see com.srbp.util.queries.AbstractQuery#getWindowSlide()
     */
    @Override
    public String getWindowSlide() {
        return "5s";
    }

    /* (non-Javadoc)
     * @see com.srbp.util.queries.AbstractQuery#getBGPAsString()
     */
    @Override
    public String getBGPAsString() {
        return "srbp:person1 srbp:isIn " + Query.BGP_VARIABLE_STRING + " ";
    }

    @Override
    public String toString() {
        return id;
    }
}
