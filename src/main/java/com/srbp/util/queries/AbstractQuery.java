package com.srbp.util.queries;

/**
 * @author peter
 *
 */
public abstract class AbstractQuery extends Query {

    public abstract String getCQELSQueryString();

    public abstract String getCSPARQLQueryString();

    public abstract String getSparkwaveQueryString();

    public String getWindowSize() {
        return "10s";
    };

    public String getWindowSlide() {
        return "5s";
    };
	
    public abstract String getBGPAsString();
    
}

