/*
 * Copyright 2014 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.util.queries;

/**
 * Query 2: Aggregation Test; return the Average Amount of People in a Room.
 *
 * @author Bernhard
 */
public class Query2 extends AbstractQuery {

    private String id = "Query2";

    @Override
    public String getCQELSQueryString() {
        return "PREFIX srbp: <http://www.streamreasoning.org/ontologies/sr4ld2013-onto#> "
                + "SELECT AVG(?person) "
                + "WHERE { "
                + Query.STREAM_URI_AND_BGP_DUMMY_STRING
                + "}";
    }

    @Override
    public String getCSPARQLQueryString() {
        return "REGISTER QUERY TrackPersonSliding AS "
                + "PREFIX srbp: <http://www.streamreasoning.org/ontologies/sr4ld2013-onto#> "
                + "SELECT AVG(?person) "
                + Query.STREAM_URI_DUMMY_STRING
                + "WHERE { "
                + "{ "
                + Query.BGP_DUMMY_STRING
                + "}}";
    }

    @Override
    public String getSparkwaveQueryString() {
        return "PREFIX srbp: <http://www.streamreasoning.org/ontologies/sr4ld2013-onto#>\n"
                + "EPSILON_ONTOLOGY = \"target/classes/com/srbp/ontology/rsp2014-onto_mod.ttl\"\n"
                + "STATIC_INSTANCES = \"null\"\n"
                + "HANDLERS {\n"
                + " HANDLER {\n"
                + "     \"class\" = \"com.srbp.util.listener.SparkwaveResultHandler\"\n"
                + " }\n"
                + "}\n"
                + "CONSTRUCT {\n"
                + Query.BGP_DUMMY_STRING
                + "\n} WHERE {\n"
                + Query.BGP_DUMMY_STRING
                + "\n TIMEWINDOW (10000)\n"
                + "}";
    }

    @Override
    public String getBGPAsString() {
        return "srbp:person1 srbp:isIn ?room";
    }

    @Override
    public String toString() {
        return id;
    }

}
