package com.srbp.util.queries;

/**
 * @author peter
 *
 */
public class Query  {

	public static final String STREAM_URI_DUMMY_STRING = "STREAM_URI_DUMMY_STRING";
	public static final String BGP_DUMMY_STRING = "BGP_DUMMY_STRING";
	public static final String STREAM_URI_AND_BGP_DUMMY_STRING = "STREAM_URI_AND_BGP_DUMMY_STRING";
	public static final String BGP_VARIABLE_STRING = "BGP_VARIABLE_STRING ";

}
