/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srbp.engines;

import com.srbp.util.listener.IMeasurmentListener;
import com.srbp.util.listener.IResultListener;
import com.srbp.util.queries.AbstractQuery;
import com.srbp.util.queries.AvailableQueries;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Interface that each Engine has to implement. Each engine runs on separate
 * thread.
 *
 * @author Bernhard
 * @author peter
 */
public interface IEngine extends Runnable {

    /**
     * Executes a query on the underlying engine.
     *
     * @param qry
     */
    void executeQuery(String qry);

    /**
     * Registers a query at the underlying engine.
     *
     * @param query
     * @throws java.text.ParseException
     */
    void registerSelectQuery(final String query, ArrayList<String> streamurilist) throws ParseException;

    void registerEventListener(final IResultListener listener);

    void registerMeasurementListener(final IMeasurmentListener listener);

    /**
     * Stops the thread of the engine.
     */
    void stopThread();

    /**
     * Starts the engine on a separate thread.
     */
    void startThread();

    /**
     * Interrupts the engine.
     */
    void interrupt();

	/**
	 * @param streamURIList
	 * @param query1
	 * @return
	 */
	String getQuery(ArrayList<String> streamURIList, AbstractQuery query);

}
