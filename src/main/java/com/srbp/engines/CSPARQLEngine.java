package com.srbp.engines;

import com.srbp.util.listener.IContinuousListener;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.srbp.datagen.model.RdfQuadruple;
import com.srbp.datagen.model.RdfQuadrupleQueue;
import com.srbp.service.MechanicsService;
import com.srbp.util.listener.IMeasurmentListener;
import com.srbp.util.listener.IResultListener;
import com.srbp.util.listener.RDFEvent;
import com.srbp.util.queries.AbstractQuery;
import com.srbp.util.queries.AvailableQueries;
import com.srbp.util.queries.Query;
import com.srbp.util.queries.Query1;

import eu.larkc.csparql.cep.api.RdfStream;
import eu.larkc.csparql.common.RDFTable;
import eu.larkc.csparql.common.RDFTuple;
import eu.larkc.csparql.core.engine.CsparqlEngine;
import eu.larkc.csparql.core.engine.CsparqlEngineImpl;
import eu.larkc.csparql.core.engine.CsparqlQueryResultProxy;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Stub for C-SPARQL
 *
 * @author Bernhard, Maxim Kolchin, Marcus
 */
public class CSPARQLEngine implements IEngine, Runnable {

	private final RdfQuadrupleQueue queue;
	private final CsparqlEngine engine = new CsparqlEngineImpl();
	private final Map<String, RdfStream> streams = new HashMap<>();
	private final AtomicInteger througput = new AtomicInteger(0);

	private final static transient Logger log = LoggerFactory.getLogger(CSPARQLEngine.class);

	private List<CsparqlQueryResultProxy> registeredQueries = new ArrayList<>();
	private List<IMeasurmentListener> regMesListener = new ArrayList<>();
	private Thread thread;

	public CSPARQLEngine() {
		this.queue = RdfQuadrupleQueue.getInstance();
		engine.initialize();
	}

	@Override
	public void executeQuery(String query) {
		try {
			engine.registerQuery(query);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void registerSelectQuery(String query, ArrayList<String> streamurilist) throws ParseException {
		log.info("registering a query...");
		registeredQueries.add(engine.registerQuery(query));
	}

	@Override
	public void registerEventListener(IResultListener listener) {
		for (CsparqlQueryResultProxy proxy : registeredQueries) {
			proxy.addObserver(new CSPARQLObserver(CSPARQLObserver.SELECT, listener));
		}
	}

	@Override
	public void run() {
		while (true) {
			RdfQuadruple quad = queue.get(0);
			if (quad != null) {
				String iri = quad.getIri();
				// log.info("IRI at engine " + iri);
				if (!streams.containsKey(iri)) {
					RdfStream newStream = new RdfStream(iri);
					engine.registerStream(newStream);
					streams.put(iri, newStream);
				}
				RdfStream stream = streams.get(iri);
				stream.put(new eu.larkc.csparql.cep.api.RdfQuadruple(quad.getSubject().toString(), quad.getPredicate().toString(), quad
						.getObject().toString(), quad.getTimestamp()));

				for (IMeasurmentListener list : regMesListener) {
					list.fireEvent(RDFEvent.QUEUESIZE, queue.size());
					list.fireEvent(RDFEvent.THROUGHPUT, 1);
				}
			}
		}
	}

	@Override
	public void stopThread() {
		if (thread != null && thread.isAlive()) {
			thread.stop();
			// TODO: further gracefull shutdown of c-sparql (see csrbench for
			// example). unregisterquery, etc.
			thread = null;
		}
	}

	@Override
	public void startThread() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void interrupt() {
		if (thread != null && thread.isAlive()) {
			thread.interrupt();
		}
	}

	@Override
	public void registerMeasurementListener(IMeasurmentListener listener) {
		regMesListener.add(listener);
	}

	/**
	 * Un-necessary, because engine is already an observer.
	 */
	public static class CSPARQLObserver implements Observer {

		private final IResultListener listener;
		private final String type;

		public static final String SELECT = "select";
		public static final String CONSTRUCT = "construct";

		public CSPARQLObserver(String type, IContinuousListener listener) {
			this.listener = (IResultListener) listener;
			this.type = type;
		}

		@Override
		public void update(Observable o, Object result) {
			RDFTable r = (RDFTable) result;
			if (type.equalsIgnoreCase(CONSTRUCT)) {
				for (final RDFTuple t : r) {
					listener.updateTriple(new Triple(toNode(t.get(0)), toNode(t.get(1)), toNode(t.get(2))));
				}
			} else {
				String[] names = r.getNames().toArray(new String[] {});
				for (RDFTuple t : r) {
					BindingHashMap binding = new BindingHashMap();
					for (int i = 0; i < names.length; i++) {
						Var curVal = Var.alloc(names[i]);
						binding.add(curVal, toNode(t.get(i)));
					}
					listener.updateMapping(binding);
				}
			}
		}

		private Node toNode(String node) {
			if (node.startsWith("<")) {
				return Node.createURI(node);
			} else {
				return Node.createLiteral(node);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.srbp.engines.IEngine#getQuery(java.util.ArrayList,
	 * com.srbp.util.queries.AvailableQueries)
	 */
	@Override
	public String getQuery(ArrayList<String> streamURIList, AbstractQuery query) {
		StringBuilder sb = new StringBuilder();
		for (String streamURI : streamURIList) {
			// yields: FROM STREAM <streamURI> [RANGE windowSize s STEP
			// windowStep s]
			sb.append("FROM STREAM <");
			sb.append(streamURI);
			sb.append("> ");
			sb.append("[RANGE ");
			sb.append(query.getWindowSize());
			sb.append(" STEP ");
			sb.append(query.getWindowSlide());
			sb.append("]");
			sb.append(System.getProperty("line.separator"));
		}
		String queryString = query.getCSPARQLQueryString().replace(Query.STREAM_URI_DUMMY_STRING, sb.toString());
		queryString = queryString.replace(Query.BGP_DUMMY_STRING, query.getBGPAsString());
		queryString = queryString.replace(Query.BGP_VARIABLE_STRING, "?room");
		log.info(queryString);
		return queryString;
	}

}
