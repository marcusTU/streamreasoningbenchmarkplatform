/*
 * Copyright 2015 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.engines;

/**
 * This is the {@link IEngine} for E-Talis. The problem is that E-Talis is implemented
 * in Prolog. Therefore we need at least an prolog interpreter and a java bridge.
 * 
 * @see <a href="https://code.google.com/p/etalis/source/browse/jtalis/">E-Talis</a>
 * @author Bernhard
 */
public class ETalisEngine {
    
}
