/*
 * Copyright 2014 LinkedData Lab.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.engines.measurement;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This is an instrumented ThreadPool that allows to measure the memory
 * consumption of a certain Thread.
 * 
 * @author Bernhard
 */
//http://www.javacodegeeks.com/2012/03/threading-stories-about-robust-thread.html
public class InstrumentedThreadPool extends ThreadPoolExecutor {

	/**
	 * Maximum amount of threads that can run in this Pool-Executor.
	 */
	private static final int POOL_SIZE = 1;

	/**
	 * Maximum keep-Alive time before a thread is discarded.
	 */
	private static final long KEEP_ALIVE = Long.MAX_VALUE;

	/**
	 * Local Thread variable. Used for indicating the start of the engine.
	 */
	private final ThreadLocal<Long> startTime;

	/**
	 * Contains a Mapping of the Task to its request time.
	 */
	private final ConcurrentHashMap<Runnable, Long> timeOfRequest
	= new ConcurrentHashMap<>();

	private long lastArrivalTime = 0L;
	/**
	 * consumed time, when the thread war running.
	 */
	private final AtomicLong totalServiceTime;

	/**
	 * Total time of the Runnable in the Thread-Pool.
	 */
	private final AtomicLong totalPoolTime;

	/**
	 * net time of the Runnable.
	 */
	private final AtomicLong aggregateInterRequestArrivalTime;

	/**
	 * Number of calls of the Thread.
	 */
	private final AtomicInteger numberOfRequests;

	/**
	 * Constructs a fixed size Thread-Pool, that allows measurement tasks. The
	 * corePoolSize/maximumPoolSize is set to one and keepAliveTime does not
	 * matter, because we have only one thread. The keepAliveTime is set to
	 * {@link Long#MAX_VALUE} and its timeUnit to {@link TimeUnit#MILLISECONDS}
	 *
	 * @see ThreadPoolExecutor#corePoolSize
	 * @see ThreadPoolExecutor#maximumPoolSize
	 * @see ThreadPoolExecutor#keepAliveTime
	 *
	 * @param threads Collection of submitted Tasks.
	 */
	public InstrumentedThreadPool(BlockingQueue<Runnable> threads) {
		super(POOL_SIZE, POOL_SIZE, KEEP_ALIVE, TimeUnit.MILLISECONDS, threads);
		startTime = new ThreadLocal<>();
		//initalize variables
		totalServiceTime= new AtomicLong(0L);
		totalPoolTime = new AtomicLong(0L);
		aggregateInterRequestArrivalTime = new AtomicLong(0);
		numberOfRequests = new AtomicInteger(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void beforeExecute(Thread t, Runnable r) {
		super.beforeExecute(t, r);
		//start time of the thread
		startTime.set(System.nanoTime());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Runnable command) {
		long now = System.nanoTime();

		numberOfRequests.incrementAndGet();
		synchronized (this) {
			if (lastArrivalTime != 0L) {
				aggregateInterRequestArrivalTime.addAndGet(now - lastArrivalTime);
			}
			lastArrivalTime = now;
			timeOfRequest.put(command, now);
		}
		super.execute(command);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		//collect diagnostic Resources.
		try {
			totalServiceTime.addAndGet(System.nanoTime() - startTime.get());
			Long remove = timeOfRequest.remove(r);
			if(remove == null){
				remove = 0L;
			}
			totalPoolTime.addAndGet(startTime.get() - remove);
		} finally {
			super.afterExecute(r, t);
		}
	}

}
