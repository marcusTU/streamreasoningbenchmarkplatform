package com.srbp.engines.measurement;

import java.util.Date;

/**
 * Domain Object for the memory usage of each engine at time t.
 *
 * @author marcus
 *
 */
public class EngineMemoryUsage {

    public String engine;
    public double memoryUsage;
    public long memoryTimestamp;

    /**
     * @param engine
     * @param memoryUsage
     */
    public EngineMemoryUsage(String engine, double memoryUsage
    ) {
        super();
        this.engine = engine;
        this.memoryUsage = memoryUsage;
        this.memoryTimestamp = System.currentTimeMillis();
    }

    /**
     * @return the engine
     */
    public String getEngine() {
        return engine;
    }

    /**
     * @param engine the engine to set
     */
    public void setEngine(String engine) {
        this.engine = engine;
    }

    /**
     * @return the memoryUsage
     */
    public double getMemoryUsage() {
        return memoryUsage;
    }

    /**
     * @param memoryUsage the memoryUsage to set
     */
    public void setMemoryUsage(double memoryUsage) {
        this.memoryUsage = memoryUsage;
    }

    /**
     * @return the memoryTimestamp
     */
    public Date getMemoryTimestamp() {
        return new Date(memoryTimestamp);
    }

    /**
     * @param memoryTimestamp the memoryTimestamp to set
     */
    public void setMemoryTimestamp(Date memoryTimestamp) {
        this.memoryTimestamp = memoryTimestamp.getTime();
    }

}
