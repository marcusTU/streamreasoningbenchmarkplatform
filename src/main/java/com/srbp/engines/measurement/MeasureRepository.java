package com.srbp.engines.measurement;

import java.io.Serializable;
import org.primefaces.model.chart.ChartSeries;

/**
 * The service that performs a measurement.
 *
 * @author Bernhard
 */
public final class MeasureRepository implements Serializable {

    private static final long serialVersionUID = 7618160211636269628L;

    private static MeasureRepository serivce = null;

    /**
     * list of all measures
     */
    private ChartSeries throughput = new ChartSeries();
    private ChartSeries queueSize = new ChartSeries();

    /**
     * Hidden constructor.
     */
    private MeasureRepository() {
    }

    /**
     * Get the singleton instance of this service.
     *
     * @return
     */
    public static synchronized MeasureRepository getInstance() {
        if (serivce == null) {
            serivce = new MeasureRepository();
        }
        return serivce;
    }

    public void setThroughputChart(ChartSeries series) {
        this.throughput = series;
    }

    public ChartSeries getThroughputChart() {
        return this.throughput;
    }

    public void setQueueSizeChart(ChartSeries series) {
        this.queueSize = series;
    }

    public ChartSeries getQueueSizeChart() {
        return this.queueSize;
    }
}
