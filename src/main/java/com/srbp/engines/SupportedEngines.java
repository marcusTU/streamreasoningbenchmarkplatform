/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srbp.engines;

/**
 * Enumeration for supported Engines.
 * @author Bernhard
 */
public enum SupportedEngines {
    /**
     * Enum for Deri's CQELS engine.
     */
    CQELS, 
    /**
     * Enum for C-Sparql Engine.
     */
    CSPARQL,
    SPARKWAVE
}
