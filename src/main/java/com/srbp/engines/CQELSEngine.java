package com.srbp.engines;

import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;

import java.util.HashMap;
import java.util.Map;

import org.deri.cqels.engine.ExecContext;
import org.deri.cqels.engine.RDFStream;

import com.srbp.datagen.model.RdfQuadruple;
import com.srbp.datagen.model.RdfQuadrupleQueue;
import com.srbp.datagen.model.DefaultRDFStream;
import com.srbp.util.listener.IMeasurmentListener;
import com.srbp.util.listener.IResultListener;
import com.srbp.util.listener.RDFEvent;
import com.srbp.util.queries.AbstractQuery;
import com.srbp.util.queries.Query;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.deri.cqels.data.Mapping;
import org.deri.cqels.engine.ContinuousListener;
import org.deri.cqels.engine.ContinuousSelect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the class for the CQELS Engines.
 *
 * @author Bernhard
 */
public class CQELSEngine implements IEngine {

    private final static transient Logger log = LoggerFactory.getLogger(CQELSEngine.class);

    /**
     * Internal Thread.
     */
    private Thread cqelsThread = null;

    /**
     * Mapping of <IRI>s to {@link RDFStream}.
     */
    private Map<String, RDFStream> streams = new HashMap<>();

    /**
     * The temporary Model of the Engine.
     */
    private ExecContext context = null;
    /**
     * The queue of streamed quads.
     */
    private RdfQuadrupleQueue queue;

    /**
     * Tracks the throughput.
     */
    private AtomicInteger througput = new AtomicInteger(0);

    /**
     * Size of the queue.
     */
    private AtomicInteger queueSize = new AtomicInteger(0);

    private List<ContinuousSelect> registeredSelects = new ArrayList<>();

    private List<IMeasurmentListener> regMesListener = new ArrayList<>();
    
    private List<CQELSSelectListener> regResultListener = new ArrayList<>();

    /**
     * constructor.
     *
     * @param path - the path of the CQELS Engine.
     */
    public CQELSEngine(String path) {
        log.info("creating cqles engine...");
        context = new ExecContext(path, true);
        queue = RdfQuadrupleQueue.getInstance();
    }

    @Override
    public void executeQuery(String qry) {
        context.registerSelect(qry);
    }

    @Override
    public void registerSelectQuery(final String query, ArrayList<String> streamurilist) {
        System.out.println("Query: "+query);
        registeredSelects.add(context.registerSelect(query));
    }

    @Override
    public void registerEventListener(IResultListener listener) {
        // if we have to sniff to the result of cqels
        for (ContinuousSelect query : registeredSelects) {
            CQELSSelectListener cqelsSelectListener = new CQELSSelectListener(context, listener);
            regResultListener.add(cqelsSelectListener);
            query.register(cqelsSelectListener);
        }
    }

    @Override
    public void registerMeasurementListener(IMeasurmentListener listener) {
        regMesListener.add(listener);
    }

    @Override
    public void run() {
        boolean runnable = true;
        while (runnable) {
            RdfQuadruple quad = queue.get(0);
            if (quad != null) {
                for (CQELSSelectListener list : regResultListener) {
                    list.streamQuadruple(quad);
                }
                String iri = quad.getIri();
                if (!streams.containsKey(iri)) {
                    streams.put(iri, new DefaultRDFStream(context, iri));
                }
                RDFStream str = streams.get(iri);
                str.stream(quad.asTriple());
                //log.info("streamed to engine: " + quad.getSubject().getLocalName() + " - " + quad.getObject().getLocalName() +" to CQELS engine");

                for (IMeasurmentListener list : regMesListener) {
                    list.fireEvent(RDFEvent.THROUGHPUT, 1);
                    list.fireEvent(RDFEvent.QUEUESIZE, queue.size());
                }

            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void stopThread() {
        if (cqelsThread != null && cqelsThread.isAlive()) {
            cqelsThread.stop();
            cqelsThread = null;
        }
    }

    @Override
    public void startThread() {
        if (cqelsThread == null) {
            cqelsThread = new Thread(this);
            cqelsThread.start();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.srbp.domain.engines.IEngine#interrupt()
     */
    @Override
    public void interrupt() {
        if (cqelsThread != null && cqelsThread.isAlive()) {
            cqelsThread.interrupt();
        }
    }

    private static class CQELSSelectListener implements ContinuousListener, EventListener {

        private final IResultListener listener;
        private final ExecContext context;

        public CQELSSelectListener(ExecContext context, EventListener listener) {
            this.listener = (IResultListener) listener;
            this.context = context;
        }

        @Override
        public void update(Mapping mapping) {
            BindingHashMap binding = new BindingHashMap();
            Iterator<Var> iter = mapping.vars();
            while (iter.hasNext()) {
                Var v = iter.next();
                binding.add(v, context.engine().decode(mapping.get(v)));
            }
            listener.updateMapping(binding);
        }

        public void streamQuadruple(RdfQuadruple quad) {
            listener.insertRawQuadruple(quad);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.srbp.engines.IEngine#getQuery(java.util.ArrayList,
     * com.srbp.util.queries.AbstractQuery)
     */
    @Override
    public String getQuery(ArrayList<String> streamURIList, AbstractQuery query) {
        StringBuilder sb = new StringBuilder();
        StringBuilder variable_sb = new StringBuilder();
        int roomNumber=0;
        for (String streamURI : streamURIList) {
        	++roomNumber;
        	// yields: STREAM <streamURI> [RANGE windowSize s SLIDE
            // windowStep s]
            sb.append("STREAM <");
            sb.append(streamURI);
            sb.append("> ");
            sb.append("[RANGE ");
            sb.append(query.getWindowSize());
            sb.append(" SLIDE ");
            sb.append(query.getWindowSlide());
            sb.append("] { ");
            //has to be done in order for the query to work.
            sb.append(query.getBGPAsString().replace("srbp:", "?").replace(Query.BGP_VARIABLE_STRING, "?room"+String.valueOf(roomNumber)));
            sb.append(" }");
            sb.append(System.getProperty("line.separator"));
            
            variable_sb.append("?room"+ String.valueOf(roomNumber));
            variable_sb.append(" ");
        }
        String queryString = query.getCQELSQueryString().replace(Query.STREAM_URI_AND_BGP_DUMMY_STRING, sb.toString());
		queryString = queryString.replace(Query.BGP_VARIABLE_STRING, variable_sb);
        log.info(queryString);
        return queryString;
    }

}
