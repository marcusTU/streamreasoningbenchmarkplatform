/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srbp.engines;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory-Method for selecting appropreate Stream engines. Convenience Method
 *
 * @author Bernhard
 */
public final class EngineFactory {

    private static final transient Logger LOG = LoggerFactory.getLogger(EngineFactory.class);

    private EngineFactory() {
    }
    /**
     * Returns a RDF-Engine. Also provides the basic set up of it.
     *
     * @see SupportedEngines
     * @see IEngine
     *
     * @param criteria
     * @return {@code IEngine}
     */
    public static IEngine getEngine(SupportedEngines criteria) {
        switch (criteria) {
            case CQELS:
                try {
                    File targetFolder = new File("resources/cqels_context");
                    if (!targetFolder.exists()) {
                        boolean mkdirs = targetFolder.mkdirs();
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("created File -> {}", mkdirs);
                        }
                    }
                    return (IEngine) new CQELSEngine(targetFolder.getAbsolutePath());
                } catch (Exception se) {
                    LOG.error(se.getMessage(), se);
                    throw se;
                }
            case CSPARQL:
                return new CSPARQLEngine();
            case SPARKWAVE:
                return new SparkwaveEngine();
            default:
                return null;
        }
    }
}
