package com.srbp.engines;

import com.srbp.util.listener.SparkwaveResultHandler;

import at.sti2.spark.core.stream.Triple;
import at.sti2.spark.core.triple.RDFLiteral;
import at.sti2.spark.core.triple.RDFTriple;
import at.sti2.spark.core.triple.RDFURIReference;
import at.sti2.spark.core.triple.RDFValue;
import at.sti2.spark.grammar.SparkParserException;
import at.sti2.spark.grammar.SparkPatternParser;
import at.sti2.spark.grammar.pattern.Pattern;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.antlr.runtime.ANTLRStringStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.sti2.sparkwave.SparkwaveNetwork;

import com.srbp.datagen.model.RdfQuadruple;
import com.srbp.datagen.model.RdfQuadrupleQueue;
import com.srbp.service.MechanicsService;
import com.srbp.util.listener.IMeasurmentListener;
import com.srbp.util.listener.IResultListener;
import com.srbp.util.listener.RDFEvent;
import com.srbp.util.queries.AbstractQuery;
import com.srbp.util.queries.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maxim Kolchin
 */
public class SparkwaveEngine implements IEngine {

	private final static transient Logger log = LoggerFactory.getLogger(SparkwaveEngine.class);
	private final AtomicInteger througput = new AtomicInteger(0);
	private final Map<String, SparkwaveNetwork> streams = new HashMap<>();
	private final RdfQuadrupleQueue queue;
	private Thread thread = null;
	private List<IMeasurmentListener> regMesListener = new ArrayList<>();

	public SparkwaveEngine() {
		this.queue = RdfQuadrupleQueue.getInstance();
	}

	@Override
	public void executeQuery(String qry) {
	}

	@Override
	public void registerSelectQuery(String query, ArrayList<String> streamurilist) throws ParseException {
		try {
			SparkPatternParser parser = new SparkPatternParser();
			Pattern pattern = parser.parse(new ANTLRStringStream(query)).getPattern();

			for (String streamURI : streamurilist) {
				SparkwaveNetwork network = new SparkwaveNetwork(pattern);
				network.init();
				streams.put(streamURI, network);
			}

		} catch (SparkParserException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void registerEventListener(IResultListener listener) {
		SparkwaveResultHandler.setListener(listener);
	}

	@Override
	public void registerMeasurementListener(IMeasurmentListener listener) {
		regMesListener.add(listener);
	}

	@Override
	public void stopThread() {
		if (thread != null && thread.isAlive()) {
			thread.stop();
			thread = null;
		}
	}

	@Override
	public void startThread() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	@Override
	public void interrupt() {
		if (thread != null && thread.isAlive()) {
			thread.interrupt();
		}
	}

	@Override
	public void run() {
		while (true) {
			RdfQuadruple quad = queue.get(0);
			if (quad != null) {
				String iri = quad.getIri();
				SparkwaveNetwork str = streams.get(iri);
				if (str != null) {
					str.activateNetwork(toTriple(quad));
					for (IMeasurmentListener list : regMesListener) {
						list.fireEvent(RDFEvent.THROUGHPUT, 1);
						list.fireEvent(RDFEvent.QUEUESIZE, queue.size());
					}
				}
			}
		}
	}

	private Triple toTriple(RdfQuadruple quad) {
		com.hp.hpl.jena.graph.Triple triple = quad.asTriple();
		RDFValue subject = RDFURIReference.Factory.createURIReference(triple.getSubject().getURI());
		RDFValue predicate = RDFURIReference.Factory.createURIReference(triple.getPredicate().getURI());
		RDFValue object;
		if (triple.getObject().isLiteral()) {
			object = RDFLiteral.Factory.createLiteral(triple.getObject().getLiteralLexicalForm(), RDFURIReference.Factory
					.createURIReference(triple.getObject().getLiteralDatatypeURI()), triple.getObject().getLiteralLanguage());
		} else {
			object = RDFURIReference.Factory.createURIReference(triple.getObject().getURI());
		}
		RDFTriple rdfTriple = new RDFTriple(subject, predicate, object);
		Triple newTriple = new Triple(rdfTriple, quad.getTimestamp(), 0L);
		return newTriple;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.srbp.engines.IEngine#getQuery(java.util.ArrayList,
	 * com.srbp.util.queries.AbstractQuery)
	 */
	@Override
	public String getQuery(ArrayList<String> streamURIList, AbstractQuery query) {
		String queryString = query.getSparkwaveQueryString().replaceAll(Query.BGP_DUMMY_STRING, query.getBGPAsString());
		log.info(queryString);
		return queryString;
	}

}
