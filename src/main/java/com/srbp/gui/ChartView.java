package com.srbp.gui;

import java.io.Serializable;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.srbp.engines.measurement.MeasureRepository;
import org.primefaces.model.chart.ChartSeries;

/**
 * ChartView of PrimeFaces.
 *
 * @author Marcus
 * @author Bernhard
 */
@Component
public class ChartView implements Serializable {

    /**
     * UUID
     */
    private static final long serialVersionUID = 7618160241636269628L;

    /**
     * log4j
     */
    private static final Logger LOG = LoggerFactory.getLogger(ChartView.class);

    /**
     */
    private LineChartModel throughputModel;

    /**
     */
    private LineChartModel queryExecutionModel;

    /**
     * The service for measurement.
     */
    private MeasureRepository measureService;

    /**
     * Constructor
     */
    public ChartView() {
        super();
        measureService = MeasureRepository.getInstance();
        createAnimatedModels();
    }

    /**
     *
     * @return
     */
    public LineChartModel getThroughputModel() {
        return throughputModel;
    }

    /**
     * Sets the Model for Throughput.
     *
     * @param throughputModel the throughputModel to set
     */
    public void setThroughputModel(LineChartModel throughputModel) {
        this.throughputModel = throughputModel;
    }

    private void createAnimatedModels() {
        throughputModel = initThroughputModel();
        throughputModel.setTitle("Throughput Chart");
        throughputModel.setAnimate(true);
        throughputModel.setLegendPosition("se");
        Axis yAxis = throughputModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        //        yAxis.setMax(10);

        queryExecutionModel = initQueueModel();
        queryExecutionModel.setTitle("Queue Chart");
        queryExecutionModel.setAnimate(true);
        queryExecutionModel.setLegendPosition("se");
        Axis yA2 = queryExecutionModel.getAxis(AxisType.Y);
        yA2.setMin(0);
        //        yA2.setMax(10);
    }

    private LineChartModel initThroughputModel() {
        LOG.info("initThroughputModel");
        LineChartModel model = new LineChartModel();
        ChartSeries series1 = measureService.getThroughputChart();
        series1.setLabel("Throughput");
        model.addSeries(series1);
        return model;
    }

    private LineChartModel initQueueModel() {
        LOG.info("initQueueModel");
        LineChartModel model = new LineChartModel();
         ChartSeries series1 = measureService.getQueueSizeChart();
        series1.setLabel("Queue");
        model.addSeries(series1);
        return model;
    }

    /**
     * Returns the queryexecutionModel that is used to display the data as
     * chart.
     *
     * @return the queryExecutionModel
     */
    public LineChartModel getQueryExecutionModel() {
        return queryExecutionModel;
    }

    /**
     * Sets the Model for QueryExecution.
     *
     * @param queryExecutionModel the queryExecutionModel to set
     */
    public void setQueryExecutionModel(LineChartModel queryExecutionModel) {
        this.queryExecutionModel = queryExecutionModel;
    }

}
