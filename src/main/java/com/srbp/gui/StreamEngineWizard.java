package com.srbp.gui;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.primefaces.event.FlowEvent;
import org.springframework.stereotype.Component;

import com.srbp.datagen.model.Configuration;
import com.srbp.service.MechanicsService;
import com.srbp.util.queries.AbstractQuery;
import javax.inject.Inject;
import org.springframework.beans.factory.annotation.Value;

/**
 * The main logic for the wizard view, also starts the benchmark via the
 * MechanicsService.
 *
 * @author Marcus
 *
 */
@Component
public class StreamEngineWizard implements Serializable {

    /**
     * UUID
     */
    private static final long serialVersionUID = 3345622051387191996L;

    @Value(value = "#{configuration}")
    Configuration configuration;
    
   @Inject
   QueryView view;

    /**
     * log4j
     */
    private static Logger log = LogManager.getLogger(MechanicsService.class);

    private boolean skip;

    private final MechanicsService mechService;

    public String duration;

    public String interArrivalTime;

    public String roomNumber;

    public String engine = null;

    public StreamEngineWizard() {
        this.mechService = new MechanicsService();
        this.roomNumber = "3";
        this.engine = "CQELS";
        this.duration = "10";
        this.interArrivalTime = "10";

    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Starts the benchmark.
     */
    public void runBenchmark() {
        FacesMessage msg = new FacesMessage("Successfully started benchmark!");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        //get Query from UI
        AbstractQuery selectedQuery = view.getAbstractQuery();
        log.info("Selected Query: "+selectedQuery);
        
        //run Mechanics
        log.info("Duration1: " + duration);
        log.info("interarrivaltime: " + interArrivalTime);
        log.info("streamEngineView:" + engine);
        log.info("Roomnumber:" + roomNumber);

        /**
         * Configuration File creation
         */
        this.configuration.setRoomNumber(Integer.valueOf(roomNumber));
        this.configuration.setPersonNumber(3);
        this.configuration.setEngine(engine);
        this.configuration.setDuration(Double.valueOf(duration));
        this.configuration.setInterArrivalTime(Integer.valueOf(interArrivalTime));
        this.configuration.setInterArrivalTimeSeed(5);
        this.configuration.setPersonChoiceSeed(5);
        this.configuration.setQuery(selectedQuery);

        /**
         * Write config file.
         */
        //TODO write config file
        //get rooms
        //get query
        mechService.runMechanics(this.configuration);
    }

    /**
     *
     * @return
     */
    public boolean isSkip() {
        return skip;
    }

    /**
     *
     * @param skip
     */
    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    /**
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return the interArrivalTime
     */
    public String getInterArrivalTime() {
        return interArrivalTime;
    }

    /**
     * @param interArrivalTime the interArrivalTime to set
     */
    public void setInterArrivalTime(String interArrivalTime) {
        this.interArrivalTime = interArrivalTime;
    }

//    /**
//     * @return the tripleSequenceTime
//     */
//    public String getTripleSequenceTime() {
//        return tripleSequenceTime;
//    }
//
//    /**
//     * @param tripleSequenceTime the tripleSequenceTime to set
//     */
//    public void setTripleSequenceTime(String tripleSequenceTime) {
//        this.tripleSequenceTime = tripleSequenceTime;
//    }
    /**
     * @return the engine
     */
    public String getEngine() {
        return engine;
    }

    /**
     * @param engine the engine to set
     */
    public void setEngine(String engine) {
        this.engine = engine;
    }

    /**
     * @return the roomNumber
     */
    public String getRoomNumber() {
        return roomNumber;
    }

    /**
     * @param roomNumber the roomNumber to set
     */
    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

}
