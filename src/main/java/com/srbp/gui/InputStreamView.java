package com.srbp.gui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * manages the input stream tab - specification of input streams.
 * @author Marcus
 *
 */
@Component
public class InputStreamView implements Serializable {

    /**
     * UUID
     */
    private static final long serialVersionUID = 3887034314625053308L;

    /**
     * log4j
     */
    private static final Logger LOG = LoggerFactory.getLogger(InputStreamView.class);

    public static String[] selectedRooms = null;
    public List<String> rooms = null;

    /**
     * initializes the rooms which can be choosen from the gui.
     */
    @PostConstruct
    public void init() {
        rooms = new ArrayList<>();
        rooms.add("Room A");
        rooms.add("Room B");
        rooms.add("Room C");
        rooms.add("Room D");
    }

    /**
     * @return the selectedRooms
     */
    public String[] getSelectedRooms() {
        if (selectedRooms == null) {
            LOG.info("creating new instance...");
            return new String[]{};
        } else {
            return selectedRooms.clone();
        }
    }

    /**
     * @param selectedRooms the selectedRooms to set
     */
    public void setSelectedRooms(String[] selectedRooms) {
        this.selectedRooms = selectedRooms.clone();
    }

    /**
     * @return the rooms
     */
    public List<String> getRooms() {
        return rooms;
    }

    /**
     * @param rooms the rooms to set
     */
    public void setRooms(List<String> rooms) {
        this.rooms = new ArrayList<>(rooms);
    }
}
