package com.srbp.gui;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.stereotype.Component;
import com.srbp.datagen.model.Configuration;
import org.springframework.beans.factory.annotation.Value;

/**
 * the implementation for the progress view component.
 *
 * @author Marcus
 *
 */
@Component
public class ProgressBarView implements Serializable {


	@Value(value = "#{configuration}")
	Configuration configuration;

	/**
	 * UUID
	 */
	private static final long serialVersionUID = -8375732607574122384L;

	public Integer progress;
	public Integer counter=1;

	/**
	 * returns the progress for the bar
	 *
	 * @return
	 */
	public Integer getProgress() {
		if (progress == null)
			progress = 0;
		else if (configuration.getDuration() == 0)
			progress = 0;
		else {
			int step = 100 / (int) configuration.getDuration();
			progress = counter*step;
			counter++;

		}
		if (progress > 100)
			progress = 100;

		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	/**
	 * if progress bar is complete the message gets shown in the gui.
	 */
	public void onComplete() {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Progress Completed"));
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

}
