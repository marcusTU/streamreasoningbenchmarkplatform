package com.srbp.gui;

import com.srbp.util.queries.AbstractQuery;
import com.srbp.util.queries.Query1;
import com.srbp.util.queries.Query2;
import com.srbp.util.queries.Query3;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;

/**
 * specifies the query selection tab in the view.
 *
 * @author Marcus
 *
 */
@Component
public class QueryView implements Serializable {

    /**
     * UUID
     */
    private static final long serialVersionUID = 203702437043717832L;

    private String query;
    private Map queries = new HashMap<String, AbstractQuery>();

    /**
     * initializes the query.
     */
    @PostConstruct
    public void init() {
        queries.put("Query 1", new Query1());
        queries.put("Query 2", new Query2());
        queries.put("Query 3", new Query3());
    }

    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @return the query
     */
    public AbstractQuery getAbstractQuery() {
        if (this.query.equals("Query1")) {
            return new Query1();
        } else if (this.query.equals("Query2")) {
            return new Query2();
        } else {
            return new Query3();
        }
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * @return the queries
     */
    public Map<String, AbstractQuery> getQueries() {
        return queries;
    }

    /**
     * @param queries the queries to set
     */
    public void setQueries(Map<String, AbstractQuery> queries) {
        this.queries = queries;
    }
}
