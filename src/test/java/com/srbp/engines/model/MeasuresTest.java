/*
 * Copyright 2014 Bernhard.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.srbp.engines.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test for Measurment Objects.
 * @author Bernhard
 */
public class MeasuresTest {
    
    public MeasuresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Tests the creation of a measurement set.
     * 
     * @see <a href="https://bitbucket.org/marcusTU/streamreasoningbenchmarkplatform/issue/10/measurment-numbers-are-the-same">JIRA-Ticket</a>
     */
    @Test
    public void testMeasurmentSet() {
//        System.out.println("Measurment-Set Test");
//        Set<Measures> measureSet = new HashSet<>();
//        for (int i = 10; i > 0; i--) {
//            QueueEmptyness qe = new QueueEmptyness("com.srbp.test");
//            measureSet.add(qe);
//        }
//        ArrayList<Integer> closeList = new ArrayList<>();
//        for(Measures m : measureSet){
//            Assert.assertFalse(closeList.contains(m.getMeasureNumber()));
//            closeList.add(m.getMeasureNumber());
//        }
    }
}
