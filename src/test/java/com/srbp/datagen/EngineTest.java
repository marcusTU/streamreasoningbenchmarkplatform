package com.srbp.datagen;

import com.srbp.datagen.model.RdfQuadruple;
import java.util.List;
//import org.apache.jena.riot.Lang;
//import org.apache.jena.riot.RDFDataMgr;
import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.srbp.engines.IEngine;

/**
 * @author peter
 *
 */
public class EngineTest {

    private List<RdfQuadruple> rdfQuadruples = null;

    @Before
    public void setUp() {
        BasicConfigurator.configure();
    }

    @After
    public void tearDown() {

    }

    /**
     * Quick Test for testing {@link IEngine} implementations.
     */
    @Test//(timeout=5000)
    @Ignore
    public void testGenerator() {
//        try {
//            RandomNumberService randGenerator = new RandomNumberService();
//
//            //1ter Case: Raumdaten generieren und in File schreiben
//            RoomDataGenerator rdg = new RoomDataGenerator(1, 1, 100L, 10, 10, 10, "http://streamreasoning.org/testIRI", "", 30d);
//            rdg.setRandomGenerator(randGenerator);
//            final Thread t = new Thread((Runnable) rdg);
//            t.start();
//
//            Thread.sleep(1000);
//            File jenaDir = new File("target/cqels/");
//            if (!jenaDir.exists()) {
//                jenaDir.mkdir();
//            }
//            IEngine engine = new CQELSEngine(jenaDir.getAbsolutePath());
//            engine.startThread();
//
//            //2ter Case: Quads lesen und anzeigen
//            Thread.sleep(50000);
//            List<Measures> emptyQueueList = MeasureService.getInstance().getEmptyQueueList();
//            List<Measures> throughputList = MeasureService.getInstance().getThroughputList();
//            CSV throughPut = new CSV();
//            try {
//                throughPut.saveToCSV(throughputList, "throughputList.csv");
//                throughPut.saveToCSV(emptyQueueList, "emptyList.csv");
//            } catch (Exception ex) {
//                Logger.getLogger(EngineTest.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } catch (InterruptedException ex) {
//        }
    }

    /**
     * reads a quad from a file.
     *
     * @param filepath
     */
    public void readQuadsFromFileAndCreateRDFQuadrupleClass(String filepath) {
//
//        Dataset dsIn = RDFDataMgr.loadDataset(filepath, Lang.NQUADS);
//        StmtIterator defGraphIterator = dsIn.getDefaultModel().listStatements();
//
//        // print static knowledge, i.e., not in a named model
//        while (defGraphIterator.hasNext()) {
//            Statement stmt = defGraphIterator.nextStatement();
//            printSPO(stmt);
//            System.out.println(" .");
//        }
//
//        // print streaming knowledge, i.e., in a named model.
//        // note: timestamp is taken as the name of the named model.
//        Iterator<String> iter = dsIn.listNames();
//        while (iter.hasNext()) {
//            String timestamp = iter.next();
//            Model m = dsIn.getNamedModel(timestamp);
//            StmtIterator siter = m.listStatements();
//            while (siter.hasNext()) {
//                Statement stmt = siter.nextStatement(); // get next statement
//                // create new RdfQuadruple
//                RdfQuadruple rdfq = new RdfQuadruple(stmt.getSubject().asNode(), stmt.getPredicate().asNode(), stmt.getObject().asNode(),
//                        Long.parseLong(timestamp));
//                this.rdfQuadruples.add(rdfq);
//            }
//
//        }
//
//        for (RdfQuadruple rdfq : this.rdfQuadruples) {
//            System.out.print(rdfq.getSubject() + " " + rdfq.getPredicate() + " " + rdfq.getObject() + " " + rdfq.getTimestamp() + " .");
//            System.out.print(System.lineSeparator());
//        }
    }

    /**
     * prints a SPO construct.
     *
     * @param stmt
     */
    private static void printSPO(Statement stmt) {
        Resource subject = stmt.getSubject(); // get the subject
        Property predicate = stmt.getPredicate(); // get the predicate
        RDFNode object = stmt.getObject(); // get the object

        System.out.print(subject.toString());
        System.out.print(" " + predicate.toString() + " ");
        if (object instanceof Resource) {
            System.out.print(object.toString());
        } else {
            // object is a literal
            System.out.print(" \"" + object.toString() + "\"");
        }

    }
}
